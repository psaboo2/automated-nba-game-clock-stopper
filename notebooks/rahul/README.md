# Rahul Worklog

[[_TOC_]]

## 2022-02-10 : Discussion with Machine Shop

We met with Gregg Bennett in the machine shop to get an idea of how they could help us with our project. He told us that we should buy a small basketball hoop with a backboard and a ball, but they can make a polycarbonate backboard if needed. If we give them the basket and sensors, they can help us mount the sensors in the appropriate locations on the backboard and create an enclosure for the PCB and clock display. We are responsible for buying the clock, which they recommended to be a maximum of 6 inches long.<br><br>


## 2022-02-17 : Discussion with TA

We met with Akshat to discuss the feasibility of our design as described in the project proposal and to ask about which color sensor we should use. He asked us questions about a few different scenarios in which our design needs to work, and we assured him that each of those scenarios will pose no issues with our current design. He also gave us feedback about our block diagram, recommending that we label more of the voltages to make it more part-specific. Finally, he told us that he will look into the color sensor (TCS3472) that we were researching to let us know if there are any issues with that particular sensor and if we should look for another one.

[Link to TCS3472 Color Sensor Datasheet](https://cdn-shop.adafruit.com/datasheets/TCS34725.pdf)<br><br>


## 2022-02-19 : Switched from Voltage Dividers to Buck Converters

After looking further into buck converters, we decided that the buck converter will work better than a voltage divider circuit to step down the DC supply voltage to the required voltage for each component. As such, we added a 12-to-5V and a 12-to-3V buck converter to our power supply subsystem.<br><br>


## 2022-02-21 : Design Document Check

We presented our a rough draft of our design document and received feedback from the course staff. 
When presenting the problem and solution overview, the course staff recommended that we include statistics for how much time is lost on average in an NBA game due to human reaction speed or include examples of specific games in which the game was egregiously influenced by human reaction speed. Inclusion of these statistics or examples would better emphasize the usefulness of our project. 

The staff also recommended that we look into other solutions outside of our proposed 3-sensor system since detection of a made shot would be the most difficult part of the project. 

We were also told to include the communication protocol that our solution uses in the block diagram and remove the ATmega328p descriptor in the "ATmega328p Microcontroller" block. The status LED should also go in the control & processing subsystem rather than the clock subsystem since it is used to test the data output of the microcontroller.

Furthermore, the course staff recommended that we look into using linear voltage regulators instead of buck converters since they are easier to implement and the improved efficiency provided by buck converters is not required for our project due to the stability of the output of the 12 V DC power supply.

If we stick with our current 3-sensor solution (optical, ultrasonic and color sensor), then the staff approved of the following physical design from our design document rough draft:

![Physical Design Image](physical_design.png)
<p align = "center">
Figure 1. Physical Design as of 02-21-2022.
</p>

Finally, the staff mentioned that the requirements for the R&V table for our sensors subsystem should be changed to bigger picture requirements rather than the specific output voltages of the sensors that we are using.<br><br>


## 2022-02-24 : Change of Design for Final Design Document

Based on the feedback from the design document review, we determined that the solution we presented would be too difficult to implement with enough accuracy, so we changed our entire design. Instead of using an ultrasonic sensor, optical sensor and color sensor to detect the ball going from above the rim to below the rim, we decided to change to a system with five ambient light sensors. Four sensors would be placed in 90 degree increments on the inner circle of the net below the rim, which would detect the ball in the net for a made basket. The fifth sensor would be placed at the bottom of the net in order to know when to stop the clock. All changes have been reflected on the submitted final version of the design document and the updated physical design is below.
<br>

![Physical Design Image](phys_design_2.png)
<p align = "center">
Figure 2. Physical Design as of 02-24-2022.
</p><br><br>

## 2022-02-25 : Discussion with TA

We met with Akshat to discuss the new design for our project and specifically to ask about what types of ambient light sensors we could use. When we were researching light sensors, we saw SMD components that were small enough to not interfere with a made shot in a basketball game, but we wanted to explore the possibility of a more polished solution than dangling SMD components. We also mentioned another type of light sensor we saw, but that would be too big and could interfere with a made shot. Akshat told us that he would look into potential ambient light sensors that we could use for our project.<br><br>

## 2022-03-05 : Addition of Sixth Light Sensor Connector to PCB
We decided to add a sixth light sensor connector to our PCB in case we need a second light sensor at the bottom of the net to detect the ball passing through the bottom of the net. We still believe that one sensor should be enough at the bottom due to the dimensions of the basketball and the bottom of the net. However, since I2C communication utilizes the same SDA and SCL data lines for all sensors, we decided it would be a simple proactive backup measure to add the extra connector to give us more flexibility in the event that our initial design fails to work.

## 2022-03-08 : Discussion with Machine Shop
We met with the machine shop to discuss the revised design for our project. Specifically, we talked about potential ways to mount the ambient light sensors onto the net and confirmed that we should get a transparent enclosure for our PCB and clock display. The person we spoke to mentioned that the shop could make a square backing with 4 connection points for each sensor that can be placed at the intersection of two net strings. We think this will be the best way to get the wires through the net to our sensors while keeping our sensors mounted on the net.

## 2022-03-25 : New Backplate Idea for Ambient Light Sensors
After picking up our ambient light sensors and assembling the hoop, we realized that the original backplate idea that we created with the machine shop would not work to mount the light sensors onto the net due to the thickness of the net strings compared to the small size of the sensors. Thus, we came up with a new idea to surface-mount the ambient light sensors onto a PCB that is slightly larger than the sensor itself, which would then be mounted on a backplate on the other side of the net.

## 2022-03-28 : Schematic and PCB Created for New Backplate
We finished creating the schematic and PCB design for the new backplate idea for the ambient light sensors. We sent the gerber files to Akshat (our TA) to place an order during the second round of PCB orders.

## 2022-04-06 : I2C Problem Recognized and Solved
We recognized that the five ambient light sensors we purchased (LTR-329ALS-01) would all have the same address for I2C communication without a readily available option in the datasheet to change the address. The problem with this is that when the microcontroller tries to receive data from one particular sensor, all of the sensors will write to the SDA line, which would cause an unintelligible value for the microcontroller to read. To solve this problem, we decided to order an I2C multiplexer that will connect the "universal" SDA and SCL lines for our project to the SDA and SCL lines of the particular sensor that the microcontroller is communicating with. The image of the multiplexer we plan to use is below.

![I2C Multiplexer](i2c_multiplexer.png)
<p align = "center">
Figure 3. I2C Multiplexer.
</p><br><br>

We plan to connect the SDA and SCL lines of each given ambient light sensor (sensor _n_) to SDA<sub>n</sub> and SCL<sub>n</sub>, and the microcontroller's SDA and SCL lines to the SDA and SCL pins that are third and fourth from the top left of the multiplexer shown in Figure 3. The microcontroller would first need to write to the multiplexer to select the channel (SDA<sub>n</sub> and SCL<sub>n</sub>) that the SDA and SCL lines from the microcontroller will connect to. Then, the microcontroller will communicate with that light sensor using the address provided on its datasheet, and this process of selecting the channel and communicating with the light sensor will repeat for each ambient light sensor. Since the I2C address of the four-digit seven-segment display we plan to use for the game clock is adjustable, we don't anticipate needing to connect the game clock to the multiplexer. However, in the event that some conflict arises, we can easily disconnect the game clock from the PCB and connect it to the multiplexer instead.

## 2022-04-09: I2C Multiplexer Connectors Soldered
We completed soldering the connectors onto the I2C multiplexer. After the main PCB and "mini" PCBs are soldered, we can begin making the connections to the I2C multiplexer.

## 2022-04-10: Ambient Light Sensor Communication Function Completed
We finished programming the function that lets the microcontroller retrieve data from the ambient light sensor. This function accepts a channel number as the input, which corresponds to the channel being selected on the I2C multiplexer (and thereby the light sensor that is being communicated with), and returns the light intensity data from that light sensor. Within the main loop, we call this function five times to repeatedly get data from each of the five light sensors. We now have to figure out how to program the four-digit seven-segment display to show a clock that counts down and stops when a certain threshold is met in two of the top four light sensors and then the bottom light sensor, indicating that the ball has just passed through the net.

## 2022-04-11: Difficulty Programming the Game Clock
After programming the communication with the ambient light sensors to continuously receive data from each sensor, we faced a challenge when attempting to program the four-digit seven-segment display that we plan to use as the game clock. We first created the logic that states that if at least two of the top four light sensors are triggered as well as the bottom sensor, then a function called clock_stop() is called, which will stop the clock. However, we had to make sure that the clock was running in the first place, so we looked at the datasheet for the display and found that the most reasonable way to run the clock was to print decreasing numbers on the display with a delay of 0.1 seconds between each number, resembling an NBA game clock. The issue we encountered was simultaneously running the clock and reading sensor data, since multi-threading is not supported. The straightforward solution would be to have two microcontrollers, with one running the clock and the other getting data from the light sensors, but we did not create our main PCB with space for two microcontrollers. Due to this, our plan is to test how long the function that retrieves data from the sensors takes to run five times and add to that delay time when programming the displayed number to decrease.

## 2022-04-13: Discussion with the Machine Shop
We went to the machine shop and talked about creating backplates for all five "mini" PCBs associated with the light sensors to mount them onto the net. Our plan is to use hot glue to sandwich the net with a mini PCB one one side glued to a backplate on the other side. We believe that this will provide enough support for the mini PCBs to stay mounted on the net as a ball rolls through the net and we will ensure that the PCB doesn't rotate around the net in the process of a made shot, since that would cause the light sensor to face the wrong direction. We should be receiving the backplates on 04/14, and we can then begin mounting the PCBs and testing for sturdiness.

## 2022-04-14 : Received Backplates for Mini PCBs
We received the backplates for the mini PCBs from the machine shop. After testing a light sensor individually to ensure that the I2C communication with the microcontroller works as intended, we will begin hot gluing the mini PCBs to the backplates to mount them on the net, as described in the previous section of this notebook.

## 2022-04-19 : Power Subsystem Verified
We tested the power subsystem for the first PCB using the procedure we created in our R&V table. We confirmed that the power subsystem works as intended, with the DC power supply providing a constant 12V to the linear regulator, which provides an output voltage of 3.3 V within a margin of error specified in our R&V table. Each component gets 3.3V to its respective VDD pin. All of these voltage values were tested with the multimeter in the lab. 

![Multimeter](Multimeter.png)
<p align = "center">
Figure 4. Multimeter reading of output voltage from linear regulator.
</p><br><br>

## 2022-04-20 : Completed Soldering Clock to I2C Backpack and Tried Programming Microcontroller
We completed soldering the clock to its I2C backpack. When trying to upload our program to the microcontroller, we came across several errors. We realized that many of these errors were compilation errors from the code we wrote, but after fixing those errors and pressing "burn bootloader", we ran into other errors. At first, we thought that these errors would be the result of bad soldering with the microcontroller. However, we ensured continuity between the ISP pins and the corresponding microcontroller pins with the multimeter in the lab. We also ensured that the other pins of the microcontroller were soldered down correctly by ensuring continuity between those pins and the component that the wires from those pins were connected to. This makes us believe that it is an issue with our code that we will continue debugging in the coming days.

## 2022-04-21 : Completed Soldering Second PCB
After speaking with the professor and our TA, we decided to change our initial plan to circumvent the issue when programming the game clock (see 2022-04-11). We decided to split up the tasks of running the game clock and getting data from the sensors into two separate microcontrollers so that those tasks could run simultaneously. We had spare PCBs from our first order as well as other spare parts that we ordered in the event of an issue, so we were able to solder these spare parts onto a spare PCB to essentially duplicate our first PCB, but with the second PCB running a different program. However, when connecting the cable to program the second microcontroller, we ran into a similar issue when pressing "burn bootloader" as we did with the first PCB. Again, we checked the connections between all components with a multimeter in the lab to ensure that there was no soldering mistake, and confirmed that the error must be due to a software issue. In this process, the power subsystem was verified for the second PCB. We are currently working to solve this problem for both PCBs.

## 2022-04-22 : Fixed Programming Microcontroller Issue for Sensors PCB
After re-soldering the microcontroller on the sensors PCB and ensuring that all pins were connected with a multimeter, we were able to burn bootloader and upload the program that gets the data from the light sensors successfully to the microcontroller. However, we still encounter errors when burning bootloader and uploading the clock program to the clock PCB. We realized that this code doesn't even compile correctly, so we are working on figuring out the reason that it doesn't compile. We tried using example code given with the clock display, but even that code encounters the same error. Burning the bootloader for the sensors program onto the clock PCB also produces an error, which means that we have both hardware and software issues with the clock part of the control & processing subsystem.

## 2022-04-23 : Fixed Programming Microcontroller Issues for Clock PCB
Regarding the software and hardware problems previously mentioned for the clock PCB, we were able to solve the software problem by using a different library that supports communication with an ATtiny45V. It turns out that the previous library we were trying to use to communicate with the clock was meant for complete Arduino Uno boards and did not directly support ATtiny45V chips. After solving this software issue, we moved onto the hardware problem. Since we knew that the code successfully compiles, we could tell if we had a soldering issue with the clock microcontroller by attempting to burn bootloader onto the sensors microcontroller that we knew was soldered correctly and seeing if the bootloader burned successfully. From this experiment as well as burning the bootloader for the sensors code successfully onto the clock microcontroller, we learned that the issue we faced was not with the incorrect soldering of the clock microcontroller. The only logical explanation was that the denoising capacitor value needed to change. From prior course theory, we knew that adding a capacitor in parallel would increase the capacitance of the denoising capacitor, potentially providing a more stable (although more noisy) voltage to the microcontroller at low (DC) frequencies. Thus, we added a 2.2 uF capacitor in parallel with the capacitor that was already on the PCB, and this enabled us to burn the bootloader for the clock program onto the clock PCB. However, when running an example test program on the clock microcontroller, the clock display did not show the expected numbers, which means there is another hardware issue that requires debugging.

![PCB with Added Capacitor](Added_Capacitor.png)
<p align = "center">
Figure 5. Clock PCB with added 2.2 uF capacitor in parallel to fix the microcontroller programming issue.
</p><br><br>

## 2022-04-24 : Fixed Soldering Issue with Clock and Verified Clock Subsystem
We realized that we soldered the clock incorrectly onto its I2C backpack, which is what caused the example test program to not display the expected values on the clock display. We spent much of the day desoldering all of the pins that connected the clock display to the I2C backpack so that we could re-solder the entire clock with a different orientation. After re-soldering the clock onto the I2C backpack in the correct manner, we were able to successfully run the test program to display to the clock. Then, we uploaded the actual program that continuously ran the clock and stopped it when it receives a logical "high" from an input pin. With this code working, we were able to completely verify the clock subsystem as explained in our R&V tables using the DC power supply to provide "low" and "high" voltages to the input of the clock microcontroller.

## 2022-04-25 : Switched to Photoresistor Voltage Divider-Based Light Sensing and Verified Sensors and Control & Processing Subsystems
After numerous days of attempting to debug the program that communicates between the microcontroller and I2C light sensors, we were ultimately not able to receive an "acknowledge" signal from any light sensor after writing its address on the SDA line. At this point, we decided to move to our backup plan of using photoresistors with voltage divider circuits to feed analog voltage values between 0 and VDD to the ADC pins of the sensors microcontroller depending on the ambient lighting conditions. See the image below for the planned schematic.

![PR Divider](pr_divider.png)
<p align = "center">
Figure 6. Photoresistor Voltage Divider Circuit Schematic.
</p><br><br>

Unfortunately, we realized that due to our PCB design, the number of ADC pins we could use were limited, so we ended up implementing a one-sensor system at the bottom of the net. The analog voltage output of this sensor would feed into an ADC input on the microcontroller, which would quantize this voltage to a 10-bit value. We then set a threshold in the code based on the ambient lighting conditions. If the voltage is less than that threshold, then we send a logical "high" signal to the clock microcontroller that tells it to stop the clock. Otherwise, we send logical "low" signals that tell the clock microcontroller to keep the clock running. After rigorous testing with made and missed shots from multiple angles, we determined that this one-sensor system was very accurate.

## 2022-04-26 : Demonstration Completed and Future Designs Considered
We demonstrated our working completed project in front of Prof. Schuh and Akshat (our TA). We discussed the current state of our project as well as design considerations for a hypothetical future with this project. Prof. Schuh brought up the idea of using IR sensors since our I2C ambient light sensors were not working, and this is something that we can look further into in the future as we continue developing this project. We also talked with Prof. Schuh about design considerations for a real NBA game in which there are two systems implemented on two separate basketball hoops, and how we would have to make the systems consistent with one another and calibrated for the lighting conditions of an NBA stadium.
