# Pranav Worklog

[[_TOC_]]

## 2022-02-01 : Initial Visual Aids for Project Proposal

After the TAs were inquiring about our project proposal and how the sensor placement would look, we sketched out some basic images of the system. These can be seen below:

![Visual aid of original sensor placement](sensor_placement_front_view.jpg)
<p align = "center">
Figure 1. Sensor placement mockup as of 2022-02-01.
</p>

Around the same time, we were also looking into the usage of Computer Vision as a replacement for the thermal sensor in the previous image. This design would also include a Time of Flight (ToF) sensor instead of the IR sensor. An image can be seen below:

![Computer vision placements](computer_vision_sensor_placement_front_view.jpg)
<p align = "center">
Figure 2. Computer vision and ToF placement mockup as of 2022-02-01.
</p>

## 2022-02-02 : Demo Decisions

Upon an inquiry from Professor Schuh about how we would demonstrate our system, we decided that we would design our system for a toy hoop instead of an NBA-sized hoop. This is because we believe our system will be scalable to a larger ball and larger hoop if we can demonstrate its functionality on a smaller sized hoop first. 

## 2022-02-10 : Machine Shop Initial Meeting

We met with Gregg Bennett in the machine shop before the deadline so that we could introduce ourselves and see how we could receive help for our project. Initially, we thought we would need to build a full-sized hoop and backboard. Gregg said that we should buy a toy basketball hoop that we can mount on a door, and if necessary they can use polycarbonate to create us a clear backboard for better cohesion with our sensors. The meeting ended with us explaining our sensor setup and learning that the machine shop could help us to mount them if necessary and also create an enclosure for our PCB and clock. <br><br>

## 2022-02-10 : Project Proposal Submission

For our project proposal, the design of the project shifted from our original thoughts with Computer Vision and the ToF sensor. Specifically, we retained the optical sensor but replaced the other two sensors with a camera and an ultrasonic sensor. The new design is shown below:

![Project proposal visual aid](project_proposal_visual_aid.png)
<p align = "center">
Figure 3. Project proposal mockup as of 2022-02-10.
</p>

Shortly after submission, however, we decided that the camera was just doing the same thing as a color sensor would, so that will be the next design that we utilize.

## 2022-02-17 : Discussion with TA

We met with our TA, Akshat, to introduce ourselves and talk about our design. This meeting was also to confirm that our design could work as we had outlined in the project proposal. We asked for some advice on the color sensor and which one we should use, since we haven't used one before and weren't sure of what specifications to look for. He told us he would look into the color sensor (TCS3472) and get back to us.

[Link to TCS3472 Color Sensor Datasheet](https://cdn-shop.adafruit.com/datasheets/TCS34725.pdf)<br>

Akshat also asked us to update our block diagram before we submit our first design document, since there were some flaws that we had overlooked with labeling. 

## 2022-02-19 : Switched from Voltage Dividers to Buck Converters

After looking further into buck converters, we saw that they have far higher efficiency than voltage dividers. As such, we decided to add in a 12-to-5V and a 12-to-3V buck converter into our power supply subsystem.

## 2022-02-21 : Design Document Check

We presented a rough draft of our design document to the course staff.

The staff recommended that we should provide evidence to support our statement of the problem, by specifically citing videos or games where the problem existed and could have led to a different outcome. 

They also recommended that we stay away from ultrasonic sensors because of how inaccurate they can be and how much noise they can pick up. Also, they gave us some concerns about the color sensor detecting a different color based on how close the ball is to the sensor as it passes through the net. They clarified that the problem we are trying to solve is how to detect a made shot, since once we can do that the stopping of the clock would be easy.

Furthermore, the course staff recommended linear voltage regulators rather than buck converters. This is because the buck converters provide high efficiency at the cost of complexity and power supply noise. 

## 2022-02-24 : Change of Design for Final Design Document

The feedback from the Design Document review made us realize that we should switch over to a design that can be mounted on the net. This would effectively switch the problem to one of proximity detection. Looking into the different sensors we could use, came across ambient light sensors and realized that as the ball passes through the net, the blockage of the ambient light could indicate that a ball is there. The system consists of five ambient light sensors. Four sensors would be placed in 90 degree increments on the inner circle of the net below the rim, which would detect the ball in the net for a made basket. The fifth sensor would be placed at the bottom of the net in order to know when to stop the clock. All changes have been reflected on the submitted final version of the design document and the updated physical design is below.
<br>

![Physical Design Image](phys_design_2.png)
<p align = "center">
Figure 4. Physical Design as of 02-24-2022.
</p>

## 2022-03-05 : Addition of Sixth Light Sensor Connector to PCB
While desigining our PCB, we decided to add a sixth light sensor connector, in the case that we need another sensor at the bottom of the net. This would handle the case where the ball passes on the side farthest from the original bottom sensor, potentially not "triggering" the sensor enough. By having sensors placed 180 degrees apart from each other, the ball would certainly trigger at least one of them since the net tightens toward the bottom, and it is already unlikely that the first sensor is not triggered. Adding a sensor like this is possible since we are using I2C communication and do not have to worry about needing more I/O ports.

## 2022-03-25 : New Backplate Idea for Ambient Light Sensors
After talks with the machine shop, we settled on a design for "backplates" for the sensors that would look something like this:

![Original backplate design](original_backplate_sketch.jpeg)
<p align = "center">
Figure 5. Original backplate design as of 03-25-2022.
</p><br><br>

Essentially, the backplate is what is pictured, and the wires into the sensor would be routed through the holes on the backplate. 

However, after picking up our ambient light sensors and assembling the hoop, we realized that the 2.0 mm x 2.0 mm size of the sensor would make it impractical to mount to the net and use the original backplate idea we had. Thus, we decided to design a new PCB that would host every sensor and have connections on it to effectively increase the surface area of the sensor. The PCB looks as follows:

![Sensor pcb design](sensor_pcb.png)
<p align = "center">
Figure 6. Sensor surface area PCB design as of 03-25-2022.
</p>

As you can see, the footprint of the ambient light sensor increases to roughly 18 mm x 7 mm.

## 2022-03-28 : Schematic and PCB Created for New Backplate
We finished creating the schematic and PCB design for the new backplate idea for the ambient light sensors. We sent the gerber files to Akshat (our TA) to place an order during the second round of PCB orders.

## 2022-04-06 : I2C Problem Recognized and Solved
We recognized that the five ambient light sensors we purchased (LTR-329ALS-01) would all have the same address for I2C communication without a readily available option in the datasheet to change the address. The problem with this is that when the microcontroller tries to receive data from one particular sensor, all of the sensors will write to the SDA line, which would cause an unintelligible value for the microcontroller to read. To solve this problem, we decided to order an I2C multiplexer that will connect the "universal" SDA and SCL lines for our project to the SDA and SCL lines of the particular sensor that the microcontroller is communicating with. The image of the multiplexer we plan to use is below.

![I2C Multiplexer](i2c_multiplexer.png)
<p align = "center">
Figure 7. I2C Multiplexer.
</p>

## 2022-04-09: I2C Multiplexer Connectors Soldered
We completed soldering the connectors onto the I2C multiplexer. After the main PCB and "mini" PCBs are soldered, we can begin making the connections to the I2C multiplexer.

## 2022-04-11: Difficulty Programming the Game Clock
After programming the communication with the ambient light sensors to continuously receive data from each sensor, we faced a challenge when attempting to program the four-digit seven-segment display that we plan to use as the game clock. We first created the logic that states that if at least two of the top four light sensors are triggered as well as the bottom sensor, then a function called clock_stop() is called, which will stop the clock. However, we had to make sure that the clock was running in the first place, so we looked at the datasheet for the display and found that the most reasonable way to run the clock was to print decreasing numbers on the display with a delay of 0.1 seconds between each number, resembling an NBA game clock. The issue we encountered was simultaneously running the clock and reading sensor data, since multi-threading is not supported. The straightforward solution would be to have two microcontrollers, with one running the clock and the other getting data from the light sensors, but we did not create our main PCB with space for two microcontrollers. Due to this, our plan is to test how long the function that retrieves data from the sensors takes to run five times and add to that delay time when programming the displayed number to decrease.

## 2022-04-13: Discussion with the Machine Shop
We went to the machine shop and talked about creating backplates for all five "mini" PCBs associated with the light sensors to mount them onto the net. Our plan is to use hot glue to sandwich the net with a mini PCB one one side glued to a backplate on the other side. We believe that this will provide enough support for the mini PCBs to stay mounted on the net as a ball rolls through the net and we will ensure that the PCB doesn't rotate around the net in the process of a made shot, since that would cause the light sensor to face the wrong direction. We should be receiving the backplates on 04/14, and we can then begin mounting the PCBs and testing for sturdiness. A rough sketch of the setup is below:

![Mounting of Sensor PCBs](sensor_pcb_mounting.png)
<p align = "center">
Figure 8. Sensor PCB mounting sketches.
</p><br><br>

## 2022-04-14 : Received Backplates for Mini PCBs
We received the backplates for the mini PCBs from the machine shop. After testing a light sensor individually to ensure that the I2C communication with the microcontroller works as intended, we will begin hot gluing the mini PCBs to the backplates to mount them on the net, as described in the previous section of this notebook.

## 2022-04-20 : Completed Soldering Clock to I2C Backpack and Tried Programming Microcontroller
We completed soldering the clock to its I2C backpack. When trying to upload our program to the microcontroller, we came across several errors. We realized that many of these errors were compilation errors from the code we wrote, but after fixing those errors and pressing "burn bootloader", we ran into other errors. At first, we thought that these errors would be the result of bad soldering with the microcontroller. However, we ensured continuity between the ISP pins and the corresponding microcontroller pins with the multimeter in the lab. We also ensured that the other pins of the microcontroller were soldered down correctly by ensuring continuity between those pins and the component that the wires from those pins were connected to. This makes us believe that it is an issue with our code that we will continue debugging in the coming days.

## 2022-04-21 : Completed Soldering Second PCB
After speaking with the professor and our TA, we decided to change our initial plan to circumvent the issue when programming the game clock (see 2022-04-11). We decided to split up the tasks of running the game clock and getting data from the sensors into two separate microcontrollers so that those tasks could run simultaneously. We had spare PCBs from our first order as well as other spare parts that we ordered in the event of an issue, so we were able to solder these spare parts onto a spare PCB to essentially duplicate our first PCB, but with the second PCB running a different program. However, when connecting the cable to program the second microcontroller, we ran into a similar issue when pressing "burn bootloader" as we did with the first PCB. Again, we checked the connections between all components with a multimeter in the lab to ensure that there was no soldering mistake, and confirmed that the error must be due to a software issue. In this process, the power subsystem was verified for the second PCB. We are currently working to solve this problem for both PCBs.

## 2022-04-24 : Fixed Soldering Issue with Clock and Verified Clock Subsystem
We realized that we soldered the clock incorrectly onto its I2C backpack, which is what caused the example test program to not display the expected values on the clock display. We spent much of the day desoldering all of the pins that connected the clock display to the I2C backpack so that we could re-solder the entire clock with a different orientation. After re-soldering the clock onto the I2C backpack in the correct manner, we were able to successfully run the test program to display to the clock. Then, we uploaded the actual program that continuously ran the clock and stopped it when it receives a logical "high" from an input pin. With this code working, we were able to completely verify the clock subsystem as explained in our R&V tables using the DC power supply to provide "low" and "high" voltages to the input of the clock microcontroller.

## 2022-04-25 : Switched to Photoresistor Voltage Divider-Based Light Sensing and Verified Sensors and Control & Processing Subsystems
After numerous days of attempting to debug the program that communicates between the microcontroller and I2C light sensors, we were ultimately not able to receive an "acknowledge" signal from any light sensor after writing its address on the SDA line. At this point, we decided to move to our backup plan of using photoresistors with voltage divider circuits to feed analog voltage values between 0 and VDD to the ADC pins of the sensors microcontroller depending on the ambient lighting conditions. See the image below for the planned schematic.

![PR Divider](pr_divider.png)
<p align = "center">
Figure 9. Photoresistor Voltage Divider Circuit Schematic.
</p><br><br>

Unfortunately, we realized that due to our PCB design, the number of ADC pins we could use were limited, so we ended up implementing a one-sensor system at the bottom of the net. The analog voltage output of this sensor would feed into an ADC input on the microcontroller, which would quantize this voltage to a 10-bit value. We then set a threshold in the code based on the ambient lighting conditions. If the voltage is less than that threshold, then we send a logical "high" signal to the clock microcontroller that tells it to stop the clock. Otherwise, we send logical "low" signals that tell the clock microcontroller to keep the clock running. After rigorous testing with made and missed shots from multiple angles, we determined that this one-sensor system was very accurate.

## 2022-04-26 : Demonstration Completed and Future Designs Considered
We demonstrated our working completed project in front of Prof. Schuh and Akshat (our TA). We discussed the current state of our project as well as design considerations for a hypothetical future with this project. Prof. Schuh brought up the idea of using IR sensors since our I2C ambient light sensors were not working, and this is something that we can look further into in the future as we continue developing this project. We also talked with Prof. Schuh about design considerations for a real NBA game in which there are two systems implemented on two separate basketball hoops, and how we would have to make the systems consistent with one another and calibrated for the lighting conditions of an NBA stadium.
