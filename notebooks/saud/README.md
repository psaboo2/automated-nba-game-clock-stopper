# Saud Worklog
[[_TOC_]]

## 2022-02-10: Discussion with Machine Shop
We visited the machine shop to talk to Gregg Bennett to see if he could help us build our product. Gregg mentioned that he could build a polycarbonate backboard if needed, but suggested that we should buy a clock display and a mini basketball hoop set up that has a ball, backboard, and net. Gregg mentioned that he could help us create ledges where we can place our sensors, and also create a container where we can place our pcb and clock display in.

## 2022-02-17: Meeting with TA (Akshat)
Our team met with Akshat to discuss our project proposals and questions about the design document. Akshat started the meeting by asking us some questions he had about our project proposals, mainly describing scenarios that we should consider and check to see if our proposed idea would still work. These scenerios were taken into consideration during our brainstorming of the product and we assured Akshat that the described cases would be covered by our 3 sensor system. Akshat also gave us feedback about our block diagram that was a part of the project proposal. Our team was thinking of using TCS3472 Color sensor, but we weren't sure what to look for in an ideal color sensor for our project. Akshat told us he would look into the color sensor that we were proposing and let us know of any concerns that he finds. Akshat then told us about how the design review will work, and encouraged us to make a detailed design document.

## 2022-02-19: Power Subsystem change to incoporate Buck Converters
Our team was orignally planning on using a voltage divider to provide the appropriate voltage to the necessary components, but we decided that a buck converter will work better to step-down the DC power supply voltage to 5 volts and 3 volts for the necessary components. We will now be using 2 buck converters: one 12V-to-5V and one 12V-to-3V buck converter. 

## 2022-02-21 : Design Document Check

We had our Design Document Check where we presented a rough draft of our design document to the course staff. We took note of their feedback for future revisions. The course staff recommended that we cite real examples of our problem occuring in an NBA game to build more credibility towards the problem. 

We were also told to look into alternative solutions as the course staff was worried that we would not be able to have a high accuracy in determining when a shot is made with the current proposed 3-sensor system. They also gave us tips on making sure our design document was professional and insightful. Tips like making sure we include the communication protocol (I2C) in our block diagram and to remove the techincal part names of the components and instead keep it as the general name like microcontroller. Furthermore we were told not to inlcude the LED status light in our block diagram as it would only be used for testing purposes. 

Moreover, the course staff gave us feedback on our power subsystem. They told us that we should look into using a linear voltage regulator instead of a buck converter and to research why a linear voltage regulator would be better. Which we later found out to be because linear voltage regulators are easier to implement and the impoved efficiency from the buck converters isn't necessary for our project to work. 

Our current proposed idea is shown in the following picture: 


![Physical Design Image](physical_design.png)
<p align = "center">
Figure 1. Physical Design as of 02-21-2022.
</p>

As mentioned before we were suggested to look into alternative solutions to this, but this would still be approved by the course. <br><br>


## 2022-02-24 : Design Document Changes

We made changes to our design document based on the feedback we were given and that was noted in the previous log. The first change we decided is to use a linear voltage regulator in our power subsystem. 

We also all agreed that our current design would be too hard to implement and there would be too many factors to take into account. So we decided to change the sensor system from ultrasonic sensor, optical sensor and color sensor to a system with five ambient light sensors. Four of these sensors will be placed around the upper area of the net in a circular format. The fifth sensor will be mounted at the bottom of the net to determine when the ball exits the net, which will then send a signal to stop the clock.
<br>

![Physical Design Image](phys_design_2.png)
<p align = "center">
Figure 2. Physical Design as of 02-24-2022.
</p>

## 2022-02-25 : Discussion with TA Akshat

Our team explained our new design to get feedback from Akshat. Akshat told us he would help us look for the appropriate ambient light sensor. When researching for the ambient light sensor we found components small enough that we believe it would not interupt the shot or the flow of the game. Akshat mentioned that he will also do research on his end and let us know later in the week what he found out.<br><br>



## 2022-03-05 : PCB Design
We created our PCB design so that we can solder the power subsystem and the control & processing subsytem onto the PCB while the sensors and clock subsystem will be connected through the pcb through connectors. We decided to add an additional light sensor in the off chance we would need two light sensors at the bottom. We decided to do this because the addition of an extra light sensor would only be discovered during the testing phase and at that point it may be too late to order new PCBs. This gives us some room for flexbilitiy in the case we need it.<br><br>


## 2022-03-08 : Machine Shop Visit
The team and I visited the machine shop to discuss the new design that we had in mind. We did not need too much from the machine shop other than getting ideas of how to mount the sensors onto the net. Gregg mentioned that the shop could make a square backing with 4 connection points so that we would be able to place the sensors at the intersections of the net. We decided to go with the route mentioned by the machine shop.<br><br>



## 2022-03-25 : New Backplate Idea for Ambient Light Sensors

We picked up our equipment and realized that we would need to rethink how we would mount the sensors onto the net. The ambient light sensors were thinner than we originally imagined and as a result we decided the best way would be to create new "mini" PCBs that would each be able to hold one sensor and have 4 pads to connect the wires from the original PCB to the "mini" PCBs. We would then mount these "mini" PCBs onto the net which would be slightly larger, but also not large enought to disrupt the shot. 

## 2022-03-28 : New "mini" PCBs created
We created the schematic and PCB design for the new "mini" PCB. We sent the gerber files to be approved by the TA and then get it ordered afterwards. 


## 2022-04-06 : I2C Problem with Sensors

As we were programming the ambient light sensors (LTR-329ALS-01) we came across a problem. I2C works by recognizing the address of each component to then communicate with that component. However the ambient light sensors we purchased all have the same address for I2C communications which presents a problem as we would need to read each of these sensors as separete data values to determine if two sensors have been "triggered". Since they have the same addresses this would not be possible. To solve this problem we decided to use an I2C multiplexer that we can use to separate the data values from each sensor reading. To use the multiplexer we would connect the microcontroller to the multiplexer which would then connect to the different ambient light sensors. The image of the multiplexer we plan to use is below.

![I2C Multiplexer](i2c_multiplexer.png)
<p align = "center">
Figure 3. I2C Multiplexer.
</p><br><br>

The multiplexer allows us to assign each sensor to a channel and then read them one by one. From figure 3 this means we will use different sd and sc pins for each ambient light sensor. The SDA and SCL pin would have to connect to the microcontroller. The multiplexer would only be needed for the sensors and not the clock as the clock would have a different address.<br><br>


## 2022-04-09: I2C Multiplexer Connectors Soldered
We soldered the connectors onto the I2C multiplexer. We will now solder the the rest of the components onto the main PCB and also solder the ambient light sensors onto the "mini" PCBs.<br><br>


## 2022-04-10: Completion of Ambient Light Sensor Program
We created a sensor program that will lets the micrcontroller communicate with the multiplexer, select the corresponding channel, and retrieve data from that ambient light sensor. We created a separate function that will call the channel and interpret the data to help organize the code and help us reuse code. This allows us to just call the function and input which channel we want to read from as a paramter. This function will be continously called in the main loop and the program will constantly check if two sensors are triggered and the bottom sensor is also triggered. <br><br>

## 2022-04-11: Game Clock Program Problem and Solution

To program the clock we created a timer program to see if we could display a countdown where the clock counts down tenths of second whch we programmed a 0.1 second delay into the code to make the timing accurate. We also then created an interrupt condition that would stop the clock on a logical high input. So at this point we have two test cases to test when we do upload the code. 

We did realize at this point that the ATTiny45 does not support multi-threading and that resulted in a problem for us as we needed the micrcontroller to run two program simultaneously. We needed to run one program to constantly check the sensor readings and another progam to constantly run the clock timer and then take the sensor input readings to determine if the clock should be stopped. To solve this problem we plan to repurposed an extra PCB we had and solder an extra microcontroller onto that repurposed PCB. We will use the 1st PCB to control the sensor system and the 2nd PCB to control the clock. So now we have 2 PCBs and the "mini" PCBs. We will differentiate the two PCBs by calling them "sensor" PCB and "clock" PCB. For this solution to work we would need to connect the two microcontrollers so that the "sensor" PCB microcontroller can communicate with the "clock" PCB microcontroller. This will be done by repurposing the MISO pin on both microcontroller as that pin will be unused once we upload the code into each microcontroller. So after we upload the separate codes to each microcontroller we would connect the two micrcontroller through a wire that is from the "sensor" microcontroller MISO pin to the "clock" microcontroller MISO pin. 
<br><br>

## 2022-04-13: Discussion with the Machine Shop

We visited the machine shop to request for them to make 5 mini backplates for the mini PCBs that the ambient light sensors would be mounted on. The machine shop agreed to do this and they ensured us this would be something they could do quickly. Once we received the backplate we would be able to glue the mini PCBs onto the net with the net in between the mini PCB and the backplate.
<br><br>

## 2022-04-14 : Picked up Backplates for Mini PCBs
The backplates were completed the next day and we were able to pick it up. We will test to see if the code can communicate with the sensor and then mount the sensors onto the net. <br><br>

## 2022-04-19 : Power Subsystem Verified
The soldering of all components onto the main PCB was completed and we were able to test to see that each component was reciveding roughly 3.3 V and that the linear voltage regulator was correctly taking in 12 V and outputting 3.3 V. This was all tested with a multimeter. This checks off the verification that we stated in the Power Subsystem R&V table.

![Multimeter](Multimeter.png)
<p align = "center">
Figure 4. Multimeter reading of output voltage from linear regulator.
</p><br><br>

## 2022-04-20 : Completed Soldering Clock to I2C Backpack and Tried Programming Microcontroller
We soldered the I2C backpack onto the clock. We also tried to upload the program onto the microcontroller since everything was now soldered onto the main PCBs. However we came across multiple problems when trying to burn the bootloader to one of the microcontrollers. We tested for continuoity and didn't see any concerning outputs from that. We will continue to debug this. We also tried to burn the bootloader onto our 2nd PCB and that gave us a different error that we again could not debug. We will continue to try to solve this problem as it is crucial to get this solved to test our system. 

## 2022-04-21 : Debugging the Bootloader problem
We continued to try to debug the burning bootloader problem. At this point of the project we only have the power subsystem verified and we need to solve this problem to verify all other components. Both PCBs are still not able to burn the bootloader we plan to talk to a TA to get some advice. 

## 2022-04-22 : Fixed Programming Microcontroller Issue for Sensors PCB
The TA helped us realize that we did not successfully solder one of the pins so after resoldering one of the pins we were able to successfully burn the bootloader but were not able to upload our code as came across bugs. So now we are debugging the code so that we can try to test the sensor subsystem. We also tried to reconnect the sensor PCB to to the clock so that we can test out the clock code and that also did not properly work so we will also have to debug the clock code. Furthermore the clock PCB micrcontroller still at this point can not properly burn the bootloader

## 2022-04-23 : Fixed Programming Microcontroller Issues for Clock PCB
We were able to solve the software and hardware problem of the clock PCB. The software problem was sovled by utilizing the Tiny_LEDBackpack library. The library we were using that was recommended by Adafruit was only compatable with Arduino Uno chip. This new library allowed us to write simpler code that would be compatable with the ATTiny45. At this point we know that the code compiles correctly. To ensure that the micrcontroller was soldered on correctly we uploaded the sensor code and saw that it would run properly. This meant that the soldering was not the issue. One of our team members then remembered that we needed to change the capacitor value as there was too much noise. From a prior course we know that by adding capacitors in parallel we can increase the overall capicatance and as a result help with the denoising. After this the clock displayed some segments but not what we wanted. So we continued to look for other potential hardware bugs. 

![PCB with Added Capacitor](Added_Capacitor.png)
<p align = "center">
Figure 5. Clock PCB with added 2.2 uF capacitor in parallel to fix the microcontroller programming issue.
</p><br><br>

## 2022-04-24 : Fixed Soldering Issue with Clock and Verified Clock Subsystem
The reason the clock was not displaying the expected values was because we soldered the I2C in the wrong orientation. We spent a lot of time trying to deattach the soldered backpack and then re-solder in the correct orientation. This proved to be the problem as the clock was running properly after this passing the two clock test case programs mentioned earlier in a previous log. We uploaded the actual clock progam and was able to confidently check off the verification for the clock subsystem. 

## 2022-04-25 : Switched to Photoresistor Voltage Divider-Based Light Sensing and Verified Sensors and Control & Processing Subsystems
The sensor subsystem proved to be rather difficult to debug. We spent multiple days trying to obtain an acknowledge signal from the light sensor but could not. At this point we weren't sure if these I2C ambient light sensors would be ideal and we swichted to using a photoresistor in a voltage divider circuit instead. The divider circuits will give us an analog output between 0 and 3.3 VDD to the ADC pin of the "sensor" PCB micrcontroller which we then created a program that we can compare that analog output value to a callibrated threshold value. A problem we realized during this experiment is the threshold value would need to be constantly be callibrated depending on the lighting of the room. It could have a different threshold value from room to room and depending if sunlight is coming in from the windows. Furthermore because we switched to this new photoresistor rather late we weren't able to create a new PCB adn as a result only had access to one ADC pin. As a result our 5 sensor subsystem changed to a 1 sensor subsystem. 
 See the image below for the planned schematic.

![PR Divider](pr_divider.png)
<p align = "center">
Figure 6. Photoresistor Voltage Divider Circuit Schematic.
</p><br><br>

After testing the system from different angle and situations (baseline shots, off the backboard, off the rim, missed shots, airballs) the one sensor system is still very accurate 

## 2022-04-26 : Demonstration Completed
We had our demonstration today and successfully demoed our system to Prof. Schuh and Akshat (our TA). We explained to them the process of our system and future plans we had with the project. Prof. Schuh suggested that we could look into IR sensors and that is something we plan to look into as a possible better alternative solution. We also discussed with Prof. Schuh and Akshat the rigirous testing that we would need to conduct to make sure it is not creating an unfair advantage in a real game situation. 
